# gitea.com/lunny/log v0.0.0-20190322053110-01b5df579c4e
gitea.com/lunny/log
# gitea.com/lunny/tango v0.6.0
gitea.com/lunny/tango
# github.com/Unknwon/com v0.0.0-20190321035513-0fed4efef755
github.com/Unknwon/com
# github.com/davecgh/go-spew v1.1.0
github.com/davecgh/go-spew/spew
# github.com/dsnet/compress v0.0.1
github.com/dsnet/compress/bzip2
github.com/dsnet/compress/bzip2/internal/sais
github.com/dsnet/compress/internal
github.com/dsnet/compress/internal/errors
github.com/dsnet/compress/internal/prefix
github.com/dsnet/compress
# github.com/golang/snappy v0.0.1
github.com/golang/snappy
# github.com/mholt/archiver v3.1.1+incompatible
github.com/mholt/archiver
# github.com/nwaples/rardecode v1.0.0
github.com/nwaples/rardecode
# github.com/pierrec/lz4 v2.0.5+incompatible
github.com/pierrec/lz4
github.com/pierrec/lz4/internal/xxh32
# github.com/pmezard/go-difflib v1.0.0
github.com/pmezard/go-difflib/difflib
# github.com/stretchr/testify v1.3.0
github.com/stretchr/testify/assert
# github.com/ulikunitz/xz v0.5.6
github.com/ulikunitz/xz
github.com/ulikunitz/xz/internal/xlog
github.com/ulikunitz/xz/lzma
github.com/ulikunitz/xz/internal/hash
# github.com/urfave/cli v1.20.0
github.com/urfave/cli
# github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8
github.com/xi2/xz
# golang.org/x/sys v0.0.0-20190429190828-d89cdac9e872
golang.org/x/sys/unix
# gopkg.in/fsnotify.v1 v1.4.7
gopkg.in/fsnotify.v1
# gopkg.in/yaml.v2 v2.2.2
gopkg.in/yaml.v2
