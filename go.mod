module gitea.com/lunny/gop

go 1.12

require (
	gitea.com/lunny/tango v0.6.0
	github.com/Unknwon/com v0.0.0-20190321035513-0fed4efef755
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/fsnotify/fsnotify v1.4.7 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/mattn/go-sqlite3 v1.11.0 // indirect
	github.com/mholt/archiver v3.1.1+incompatible
	github.com/nwaples/rardecode v1.0.0 // indirect
	github.com/pierrec/lz4 v2.0.5+incompatible // indirect
	github.com/stretchr/testify v1.3.0
	github.com/urfave/cli v1.20.0
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
	golang.org/x/sys v0.0.0-20190429190828-d89cdac9e872 // indirect
	gopkg.in/fsnotify.v1 v1.4.7
	gopkg.in/yaml.v2 v2.2.2
)
